import os
from setuptools import setup, find_packages
from setuptools.command.install import install
from django.conf import settings
from django.core.management import call_command

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        settings.configure('zebrafish.zebrafish.settings', DEBUG=True)
        call_command("migrate")
        install.run(self)

setup(
    name='django-genetisc',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    description='Web application for management of data about Zebrafish.',
    long_description=README,
    url='http://127.0.0.1:8000/zebrafish_app/',
    author='Tomas Dockal',
    author_email='tomas-dockal@email.cz',
    install_requires=['setuptools', 'Django'],
    cmdclass={'install': PostInstallCommand},
    scripts=['zebrafish/manage.py'],
)