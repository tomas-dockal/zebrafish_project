from django.apps import AppConfig
from django.db.models.signals import post_save
from django.dispatch import receiver




class ZebrafishAppConfig(AppConfig):
    name = 'zebrafish_app'