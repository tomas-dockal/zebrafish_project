from django.contrib import admin

from .models import Fishline, Room, Rack, Position, Stock, Substock

admin.site.register(Fishline)
admin.site.register(Room)
admin.site.register(Rack)
admin.site.register(Position)
admin.site.register(Stock)
admin.site.register(Substock)