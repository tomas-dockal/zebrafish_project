from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

class Fishline(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Room(models.Model):
    room = models.CharField(max_length=15)

    def __str__(self):
        return self.room


class Rack(models.Model):
    rackname = models.CharField(max_length=20)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    row_count = models.PositiveIntegerField(null=True)
    col_count = models.PositiveIntegerField(null=True)

    def __str__(self):
        return self.rackname

    class Meta:
        app_label = 'zebrafish_app'

    # mozna pouzit ve views pro limit range na vyplneni pozice

    def get_row_range(self):
        row_range = range(1, self.row_count)
        row_choice = tuple([(i, i) for i in row_range])
        return row_choice


class Position(models.Model):

    rack = models.ForeignKey(Rack, on_delete=models.CASCADE)
    row = models.IntegerField(null=True)
    column = models.IntegerField(null=True)

    def __str__(self):
        return "%s %s %s / %s" % (self.rack.room, self.rack.rackname, self.row, self.column)


class Stock(models.Model):
    fishline = models.ForeignKey(Fishline, on_delete=models.CASCADE)
    d_o_b = models.DateField(max_length=12)

    def __str__(self):
        return "{}   date of birth: {}".format(self.fishline, self.d_o_b)

class Substock(models.Model):
    stock = models.ForeignKey(Stock, on_delete=models.CASCADE)
    position = models.OneToOneField(Position, on_delete=models.CASCADE, primary_key=True)
    count = models.PositiveIntegerField(null=True)

    def __str__(self):
        return "{} position: {}".format(self.stock, self.position)

@receiver(post_save, sender=Rack)
def create_positions(sender, instance, created, **kwargs):
        if created:
            row_range = range(1, (instance.row_count + 1))
            col_range = range(1, (instance.col_count + 1))
            for x in row_range:
                for y in col_range:
                    Position.objects.create(rack=instance, row=x, column=y)
            print("Positions for new rack %s were created" % instance.rackname)










