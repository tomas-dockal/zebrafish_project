from django.urls import path

from .views import MultipleModelView

urlpatterns = [
    path('', MultipleModelView.as_view()),
]