from django.views.generic import TemplateView, ListView

from .models import Room, Rack, Position

class MultipleModelView(TemplateView):
    template_name = 'zebrafish_app/index.html'

    def get_context_data(self, **kwargs):
        context = super(MultipleModelView, self).get_context_data(**kwargs)
        context['rooms'] = list(Room.objects.all())
        context['racks'] = list(Rack.objects.all())
        context['positions'] = list(Position.objects.all())
        # logic *)
        return context



# def login(request):
#     pass
#
# def index(request):
#     return render(request, 'zebrafish/index.html')
